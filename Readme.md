# Snake with ncurses
Snake for the console implemented with ncurses

## Build on Linux
Run the following command
```
gcc snake.c -o snake -lncurses
```
or use the task for VS Code "Build Snake".


## How to Play
Use arrow keys to control the snake. Hit "q" to stop the program.