#include <curses.h>
#include <stdlib.h>
#include <unistd.h>

typedef enum direction
{
    dir_up = 0,
    dir_down,
    dir_left,
    dir_right
} direction_e;

typedef struct snake
{
    int len;
    int capacity;
    int head;
    int *x;
    int *y;
    direction_e dir;
    bool eat;
} snake_t;

typedef struct gamefield
{
    int width;
    int height;
    int foodX;
    int foodY;
    int border;
} gamefield_t;

void newFood(gamefield_t *g)
{
    g->foodX = rand() % (g->width - (g->border * 2 + 2)) + g->border + 1;
    g->foodY = rand() % (g->height - (g->border * 2 + 2)) + g->border + 1;
}

gamefield_t createGamefield(int width, int height)
{
    gamefield_t g;
    g.border = 1;
    g.width = width;
    g.height = height;
    newFood(&g);
    return g;
}

void drawGamefield(gamefield_t g)
{
    // draw border
    for (size_t i = g.border; i < g.width - g.border; i++)
    {
        mvaddstr(g.border, i, "#");
        mvaddstr(g.height - 1 - g.border, i, "#");
    }
    for (size_t i = g.border; i < g.height - g.border; i++)
    {
        mvaddstr(i, g.border, "#");
        mvaddstr(i, g.width - 1 - g.border, "#");
    }
    // draw food
    mvaddstr(g.foodY, g.foodX, "O");
}

snake_t createSnake(int size, int *x, int *y)
{
    snake_t s;
    // Set default direction (snake goes right)
    s.dir = dir_right;
    // Set snakes current head position
    s.head = size - 1;
    // Initialize snake with length size
    s.capacity = 1000;
    s.len = size;
    s.x = malloc(sizeof(int) * s.capacity);
    s.y = malloc(sizeof(int) * s.capacity);
    for (size_t i = 0; i < size; i++)
    {
        s.x[i] = x[i];
        s.y[i] = y[i];
    }
    return s;
}

void destroySnake(snake_t s)
{
    free(s.x);
    free(s.y);
}

void drawSnake(snake_t s)
{
    for (size_t i = 0; i < s.len; i++)
    {
        mvaddstr(s.y[i], s.x[i], "X");
    }
}
void changeDirection(snake_t *s, direction_e d)
{
    int headPrev = s->head - 1 < 0 ? s->len - 1 : s->head - 1;
    int x, y, prevx, prevy;
    prevx = s->x[headPrev];
    prevy = s->y[headPrev];
    switch (d)
    {
    case dir_up:
        x = s->x[s->head];
        y = s->y[s->head] - 1;
        break;
    case dir_down:
        x = s->x[s->head];
        y = s->y[s->head] + 1;
        break;
    case dir_left:
        x = s->x[s->head] - 1;
        y = s->y[s->head];
        break;
    case dir_right:
        x = s->x[s->head] + 1;
        y = s->y[s->head];
        break;

    default:
        break;
    }
    if (x == prevx && y == prevy)
        return;
    s->dir = d;
}

void moveSnake(snake_t *s)
{
    if (s->eat)
    {
        for (size_t i = s->len - 1; i > s->head; i--)
        {
            s->x[i + 1] = s->x[i];
            s->y[i + 1] = s->y[i];
        }
        s->len++;
        s->eat = false;
    }
    // Store previous head position
    int x = s->x[s->head];
    int y = s->y[s->head];
    // Update head position
    s->head = (s->head + 1) % s->len;
    // Overwrite value at head position
    switch (s->dir)
    {
    case dir_up:
        s->y[s->head] = y - 1;
        s->x[s->head] = x;
        break;
    case dir_down:
        s->y[s->head] = y + 1;
        s->x[s->head] = x;
        break;
    case dir_left:
        s->y[s->head] = y;
        s->x[s->head] = x - 1;
        break;
    case dir_right:
        s->y[s->head] = y;
        s->x[s->head] = x + 1;
        break;
    default:
        break;
    }
}

bool checkFood(snake_t s, gamefield_t g)
{
    return s.x[s.head] == g.foodX && s.y[s.head] == g.foodY;
}

bool checkCollision(snake_t s, gamefield_t g)
{
    int headx = s.x[s.head];
    int heady = s.y[s.head];
    // Check for head collision with snake
    for (size_t i = 0; i < s.len; i++)
    {
        if (i != s.head && headx == s.x[i] && heady == s.y[i])
        {
            return true;
        }
    }
    // Check for head collision with border
    for (size_t i = g.border; i < g.width - g.border; i++)
    {
        if ((g.border == heady || g.height - 1 - g.border == heady) && i == headx)
            return true;
    }
    for (size_t i = g.border; i < g.height - g.border; i++)
    {
        if ((g.border == headx || g.width - 1 - g.border == headx) && i == heady)
            return true;
    }
    // No collision
    return false;
}

void quit()
{
    endwin();
}

int main(void)
{

    int x = 0, y = 0;

    initscr();
    raw();
    keypad(stdscr, TRUE);
    atexit(quit);

    curs_set(0);
    noecho();
    timeout(0);

    int yStart[] = {3, 3, 3};
    int xStart[] = {3, 4, 5};
    snake_t snake = createSnake(3, xStart, yStart);

    int width, height;
    getmaxyx(stdscr, height, width);
    if (width > height)
    {
        width = height;
    }
    else
    {
        height = width;
    }

    gamefield_t gamefield = createGamefield(width, height);

    int speed = 20;
    int speedCounter = 0;
    int speedMax = 2;

    int c;
    while (true)
    {
        // Process inputs
        c = getch();

        if (c == KEY_LEFT)
            changeDirection(&snake, dir_left);
        if (c == KEY_RIGHT)
            changeDirection(&snake, dir_right);
        if (c == KEY_UP)
            changeDirection(&snake, dir_up);
        if (c == KEY_DOWN)
            changeDirection(&snake, dir_down);

        // Game logic
        if (speedCounter % speed == 0)
            moveSnake(&snake);
        if (checkCollision(snake, gamefield))
        {
            // Game over
            clear();
            drawGamefield(gamefield);
            drawSnake(snake);
            refresh();
            sleep(5);
            break;
        }
        if (checkFood(snake, gamefield))
        {
            snake.eat = true;
            newFood(&gamefield);
        }

        // Check for shutdown
        if (c == 'q' || c == 'Q')
            break;

        // Render
        clear();
        drawGamefield(gamefield);
        drawSnake(snake);
        refresh();
        // Wait
        speedCounter++;
        usleep(10000);
    }

    destroySnake(snake);

    return (0);
}